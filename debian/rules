#!/usr/bin/make -f
# -*- makefile -*-
# Copyright 2006, Kurt Roeckx <kurt@roeckx.be>

# Based on the sample file from dh-make:
# Sample debian/rules that uses debhelper.
# This file was originally written by Joey Hess and Craig Small.
# As a special exception, when this file is copied by dh-make into a
# dh-make output file, you may use that output file without restriction.
# This special exception was added by Craig Small in version 0.37 of dh-make.

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1


# These are used for cross-compiling and for saving the configure script
# from having to guess our platform (since we know it already)
DEB_HOST_GNU_TYPE   ?= $(shell dpkg-architecture -qDEB_HOST_GNU_TYPE)
DEB_BUILD_GNU_TYPE  ?= $(shell dpkg-architecture -qDEB_BUILD_GNU_TYPE)
DEB_HOST_MULTIARCH  ?= $(shell dpkg-architecture -qDEB_HOST_MULTIARCH)

ifeq ($(DEB_BUILD_GNU_TYPE), $(DEB_HOST_GNU_TYPE))
        confflags += --build=$(DEB_HOST_GNU_TYPE) --enable-maintainer-mode
else
        confflags += --build=$(DEB_BUILD_GNU_TYPE) --host=$(DEB_HOST_GNU_TYPE)
endif

ifneq (,$(filter parallel=%,$(DEB_BUILD_OPTIONS)))
    NUMJOBS = $(patsubst parallel=%,%,$(filter parallel=%,$(DEB_BUILD_OPTIONS)))
    MAKEFLAGS += -j$(NUMJOBS)
endif

DPKG_EXPORT_BUILDFLAGS = 1
include /usr/share/dpkg/buildflags.mk

config.status: configure.ac
	dh_testdir
	dh_autoreconf
ifneq ($(DEB_BUILD_GNU_TYPE),$(DEB_HOST_GNU_TYPE))
	./configure --enable-maintainer-mode \
		--$(if $(filter pkg.elfutils.nodebuginfod,$(DEB_BUILD_PROFILES)),dis,en)able-debuginfod
	$(MAKE) $(MAKEFLAGS)
	$(MAKE) clean
endif
	CFLAGS="$(CFLAGS) -O3" CXXFLAGS="-fpermissive" CPPFLAGS="$(CPPFLAGS)" LDFLAGS="$(LDFLAGS)" \
		./configure $(confflags) --prefix=/usr \
		--libdir=/usr/lib/$(DEB_HOST_MULTIARCH) \
		--program-prefix=eu- --disable-silent-rules \
		--$(if $(filter pkg.elfutils.nodebuginfod,$(DEB_BUILD_PROFILES)),dis,en)able-debuginfod

build: build-stamp
build-stamp:  config.status
	dh_testdir

	$(MAKE) $(MAKEFLAGS)

ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	# Check that those files exist.
	-ls -l /proc/$$$$/maps
	-ls -l /proc/$$$$/auxv
	-ls -l /proc/$$$$/
	-LC_CTYPE=C $(MAKE) check $(MAKEFLAGS) || { cat tests/test-suite.log; exit 1; }
	cat tests/test-suite.log
endif

	touch build-stamp

clean:
	dh_testdir
	dh_testroot
	rm -f build-stamp
	[ ! -f Makefile ] || $(MAKE) distclean
	dh_autoreconf_clean
	dh_clean

install: build
	dh_testdir
	dh_testroot
	dh_prep
	dh_installdirs

	# Add here commands to install the package into debian/tmp
	$(MAKE) install DESTDIR=$(CURDIR)/debian/tmp
	# We do not provide a libebl anymore, use libdw instead.
	rm -f debian/tmp/usr/include/elfutils/libebl.h

# Build architecture-independent files here.
build-indep: build
binary-indep: build-indep install
# We have nothing to do.

# Build architecture-dependent files here.
build-arch: build
binary-arch: build-arch install
	dh_testdir
	dh_testroot
	dh_installchangelogs ChangeLog
	dh_installdocs

	: # FIXME: package those ...
#	find debian/tmp -name '*debuginfod*' | xargs -r rm -f
#	rm -f debian/tmp/usr/share/man/man1/eu-elfclassify.1
#	rm -f debian/tmp/usr/share/man/man1/eu-readelf.1
#	rm -f debian/tmp/usr/share/man/man3/elf_*.3

	: # these are empty envvar settings ...
	rm -rf debian/tmp/usr/etc/profile.d

	dh_install --sourcedir=debian/tmp
	rm -f debian/elfutils/usr/bin/debuginfod*
	dh_missing --fail-missing --sourcedir=debian/tmp

	dh_strip
	dh_compress
	dh_fixperms
	dpkg-gensymbols -Pdebian/libdw1/ -plibdw1 -c4
	dpkg-gensymbols -Pdebian/libasm1/ -plibasm1 -c4
	dpkg-gensymbols -Pdebian/libelf1/ -plibelf1 -c4
	dh_makeshlibs -plibelf1 -V "libelf1 (>= 0.132)"
	dh_makeshlibs -plibasm1 -V "libasm1 (>= 0.132)"
	dh_makeshlibs -plibdw1 -V "libdw1 (>= 0.138)"
	dh_makeshlibs -plibdebuginfod1
	dh_installdeb
	dh_shlibdeps
	dh_gencontrol
	dh_md5sums
	dh_builddeb

binary: binary-indep binary-arch

maint-unpatch:
	bash debian/unpatch.sh debian/patches

.PHONY: build clean binary-indep binary-arch binary install maint-unpatch
